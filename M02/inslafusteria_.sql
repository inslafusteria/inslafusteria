PGDMP     ,                     y           lafusteria_bd    13.2    13.1 7    !           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            "           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            #           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            $           1262    16395    lafusteria_bd    DATABASE     i   CREATE DATABASE lafusteria_bd WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Spain.1252';
    DROP DATABASE lafusteria_bd;
                postgres    false            %           0    0    DATABASE lafusteria_bd    ACL     9   GRANT ALL ON DATABASE lafusteria_bd TO administrador_bd;
                   postgres    false    3108                        3079    16742    pgcrypto 	   EXTENSION     <   CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
    DROP EXTENSION pgcrypto;
                   false            &           0    0    EXTENSION pgcrypto    COMMENT     <   COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';
                        false    2            �            1259    17611    alumnos_asignaturas    TABLE     ~   CREATE TABLE public.alumnos_asignaturas (
    dni character varying NOT NULL,
    id_asignatura character varying NOT NULL
);
 '   DROP TABLE public.alumnos_asignaturas;
       public         heap    administrador_bd    false            �            1259    17617    asignaturas    TABLE     �   CREATE TABLE public.asignaturas (
    id_asignatura character varying NOT NULL,
    descripcion character varying NOT NULL,
    id_curso character varying NOT NULL
);
    DROP TABLE public.asignaturas;
       public         heap    administrador_bd    false            �            1259    17623    asignaturas_id_curso_seq    SEQUENCE     �   CREATE SEQUENCE public.asignaturas_id_curso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.asignaturas_id_curso_seq;
       public          administrador_bd    false    202            '           0    0    asignaturas_id_curso_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.asignaturas_id_curso_seq OWNED BY public.asignaturas.id_curso;
          public          administrador_bd    false    203            �            1259    17625    aulas    TABLE     i   CREATE TABLE public.aulas (
    id_aula character varying NOT NULL,
    descripcion character varying
);
    DROP TABLE public.aulas;
       public         heap    administrador_bd    false            �            1259    17639 
   secretaria    TABLE     �   CREATE TABLE public.secretaria (
    id_cita bigint NOT NULL,
    dni character varying NOT NULL,
    fecha character varying NOT NULL,
    razon character varying NOT NULL
);
    DROP TABLE public.secretaria;
       public         heap    administrador_bd    false            �            1259    17645    cita_conserjeria_id_cita_seq    SEQUENCE     �   CREATE SEQUENCE public.cita_conserjeria_id_cita_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.cita_conserjeria_id_cita_seq;
       public          administrador_bd    false    205            (           0    0    cita_conserjeria_id_cita_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.cita_conserjeria_id_cita_seq OWNED BY public.secretaria.id_cita;
          public          administrador_bd    false    206            �            1259    17739    contacto    TABLE     �   CREATE TABLE public.contacto (
    nombre character varying NOT NULL,
    correo character varying NOT NULL,
    asunto character varying NOT NULL,
    mensaje character varying NOT NULL
);
    DROP TABLE public.contacto;
       public         heap    administrador_bd    false            �            1259    17653    cursos    TABLE     �   CREATE TABLE public.cursos (
    id_curso character varying NOT NULL,
    descripcion character varying NOT NULL,
    id_aula character varying NOT NULL,
    horario character varying
);
    DROP TABLE public.cursos;
       public         heap    administrador_bd    false            �            1259    17659    cursos_id_curso_seq    SEQUENCE     |   CREATE SEQUENCE public.cursos_id_curso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cursos_id_curso_seq;
       public          administrador_bd    false    207            )           0    0    cursos_id_curso_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cursos_id_curso_seq OWNED BY public.cursos.id_curso;
          public          administrador_bd    false    208            �            1259    17661 
   expediente    TABLE     �   CREATE TABLE public.expediente (
    id_nota bigint NOT NULL,
    dni character varying NOT NULL,
    id_asignatura character varying NOT NULL,
    id_curso character varying NOT NULL,
    nota numeric(4,2) NOT NULL
);
    DROP TABLE public.expediente;
       public         heap    administrador_bd    false            �            1259    17667    expediente_id_nota_seq    SEQUENCE        CREATE SEQUENCE public.expediente_id_nota_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.expediente_id_nota_seq;
       public          administrador_bd    false    209            *           0    0    expediente_id_nota_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.expediente_id_nota_seq OWNED BY public.expediente.id_nota;
          public          administrador_bd    false    210            �            1259    17669    material_informatico    TABLE     �   CREATE TABLE public.material_informatico (
    id_material character varying NOT NULL,
    id_aula character varying,
    descripcion character varying
);
 (   DROP TABLE public.material_informatico;
       public         heap    administrador_bd    false            �            1259    17681    usuarios    TABLE     �  CREATE TABLE public.usuarios (
    dni character varying NOT NULL,
    nombre character varying NOT NULL,
    apellidos character varying,
    fecha_naci date NOT NULL,
    tel_contacto numeric(9,0) NOT NULL,
    correo_elec character varying NOT NULL,
    puesto character varying NOT NULL,
    cargo character varying,
    id_curso character varying,
    "contraseña" character varying
);
    DROP TABLE public.usuarios;
       public         heap    administrador_bd    false            �            1259    17687    personas_id_curso_seq    SEQUENCE     ~   CREATE SEQUENCE public.personas_id_curso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.personas_id_curso_seq;
       public          administrador_bd    false    212            +           0    0    personas_id_curso_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.personas_id_curso_seq OWNED BY public.usuarios.id_curso;
          public          administrador_bd    false    213            x           2604    17695    asignaturas id_curso    DEFAULT     |   ALTER TABLE ONLY public.asignaturas ALTER COLUMN id_curso SET DEFAULT nextval('public.asignaturas_id_curso_seq'::regclass);
 C   ALTER TABLE public.asignaturas ALTER COLUMN id_curso DROP DEFAULT;
       public          administrador_bd    false    203    202            z           2604    17697    cursos id_curso    DEFAULT     r   ALTER TABLE ONLY public.cursos ALTER COLUMN id_curso SET DEFAULT nextval('public.cursos_id_curso_seq'::regclass);
 >   ALTER TABLE public.cursos ALTER COLUMN id_curso DROP DEFAULT;
       public          administrador_bd    false    208    207            {           2604    17698    expediente id_nota    DEFAULT     x   ALTER TABLE ONLY public.expediente ALTER COLUMN id_nota SET DEFAULT nextval('public.expediente_id_nota_seq'::regclass);
 A   ALTER TABLE public.expediente ALTER COLUMN id_nota DROP DEFAULT;
       public          administrador_bd    false    210    209            y           2604    17699    secretaria id_cita    DEFAULT     ~   ALTER TABLE ONLY public.secretaria ALTER COLUMN id_cita SET DEFAULT nextval('public.cita_conserjeria_id_cita_seq'::regclass);
 A   ALTER TABLE public.secretaria ALTER COLUMN id_cita DROP DEFAULT;
       public          administrador_bd    false    206    205            |           2604    17700    usuarios id_curso    DEFAULT     v   ALTER TABLE ONLY public.usuarios ALTER COLUMN id_curso SET DEFAULT nextval('public.personas_id_curso_seq'::regclass);
 @   ALTER TABLE public.usuarios ALTER COLUMN id_curso DROP DEFAULT;
       public          administrador_bd    false    213    212                      0    17611    alumnos_asignaturas 
   TABLE DATA           A   COPY public.alumnos_asignaturas (dni, id_asignatura) FROM stdin;
    public          administrador_bd    false    201   6?                 0    17617    asignaturas 
   TABLE DATA           K   COPY public.asignaturas (id_asignatura, descripcion, id_curso) FROM stdin;
    public          administrador_bd    false    202   S?                 0    17625    aulas 
   TABLE DATA           5   COPY public.aulas (id_aula, descripcion) FROM stdin;
    public          administrador_bd    false    204   p?                 0    17739    contacto 
   TABLE DATA           C   COPY public.contacto (nombre, correo, asunto, mensaje) FROM stdin;
    public          administrador_bd    false    214   �?                 0    17653    cursos 
   TABLE DATA           I   COPY public.cursos (id_curso, descripcion, id_aula, horario) FROM stdin;
    public          administrador_bd    false    207   �?                 0    17661 
   expediente 
   TABLE DATA           Q   COPY public.expediente (id_nota, dni, id_asignatura, id_curso, nota) FROM stdin;
    public          administrador_bd    false    209   �?                 0    17669    material_informatico 
   TABLE DATA           Q   COPY public.material_informatico (id_material, id_aula, descripcion) FROM stdin;
    public          administrador_bd    false    211   �?                 0    17639 
   secretaria 
   TABLE DATA           @   COPY public.secretaria (id_cita, dni, fecha, razon) FROM stdin;
    public          administrador_bd    false    205   @                 0    17681    usuarios 
   TABLE DATA           �   COPY public.usuarios (dni, nombre, apellidos, fecha_naci, tel_contacto, correo_elec, puesto, cargo, id_curso, "contraseña") FROM stdin;
    public          administrador_bd    false    212   @       ,           0    0    asignaturas_id_curso_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.asignaturas_id_curso_seq', 1, false);
          public          administrador_bd    false    203            -           0    0    cita_conserjeria_id_cita_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.cita_conserjeria_id_cita_seq', 25, true);
          public          administrador_bd    false    206            .           0    0    cursos_id_curso_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cursos_id_curso_seq', 1, false);
          public          administrador_bd    false    208            /           0    0    expediente_id_nota_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.expediente_id_nota_seq', 20, true);
          public          administrador_bd    false    210            0           0    0    personas_id_curso_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.personas_id_curso_seq', 2, true);
          public          administrador_bd    false    213            ~           2606    17702 ,   alumnos_asignaturas alumnos_asignaturas_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.alumnos_asignaturas
    ADD CONSTRAINT alumnos_asignaturas_pkey PRIMARY KEY (dni, id_asignatura);
 V   ALTER TABLE ONLY public.alumnos_asignaturas DROP CONSTRAINT alumnos_asignaturas_pkey;
       public            administrador_bd    false    201    201            �           2606    17704    asignaturas asignaturas_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.asignaturas
    ADD CONSTRAINT asignaturas_pkey PRIMARY KEY (id_asignatura);
 F   ALTER TABLE ONLY public.asignaturas DROP CONSTRAINT asignaturas_pkey;
       public            administrador_bd    false    202            �           2606    17706    aulas aulas_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.aulas
    ADD CONSTRAINT aulas_pkey PRIMARY KEY (id_aula);
 :   ALTER TABLE ONLY public.aulas DROP CONSTRAINT aulas_pkey;
       public            administrador_bd    false    204            �           2606    17708     secretaria cita_conserjeria_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.secretaria
    ADD CONSTRAINT cita_conserjeria_pkey PRIMARY KEY (fecha);
 J   ALTER TABLE ONLY public.secretaria DROP CONSTRAINT cita_conserjeria_pkey;
       public            administrador_bd    false    205            �           2606    17710    cursos cursos_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.cursos
    ADD CONSTRAINT cursos_pkey PRIMARY KEY (id_curso);
 <   ALTER TABLE ONLY public.cursos DROP CONSTRAINT cursos_pkey;
       public            administrador_bd    false    207            �           2606    17714 .   material_informatico material_informatico_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.material_informatico
    ADD CONSTRAINT material_informatico_pkey PRIMARY KEY (id_material);
 X   ALTER TABLE ONLY public.material_informatico DROP CONSTRAINT material_informatico_pkey;
       public            administrador_bd    false    211            �           2606    17718    expediente pk_mitabla 
   CONSTRAINT     m   ALTER TABLE ONLY public.expediente
    ADD CONSTRAINT pk_mitabla PRIMARY KEY (dni, id_asignatura, id_curso);
 ?   ALTER TABLE ONLY public.expediente DROP CONSTRAINT pk_mitabla;
       public            administrador_bd    false    209    209    209            �           2606    17722    usuarios usuarios_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (dni, correo_elec);
 @   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuarios_pkey;
       public            administrador_bd    false    212    212            �           2606    17728    cursos cursos_id_aula_FK    FK CONSTRAINT     ~   ALTER TABLE ONLY public.cursos
    ADD CONSTRAINT "cursos_id_aula_FK" FOREIGN KEY (id_aula) REFERENCES public.aulas(id_aula);
 D   ALTER TABLE ONLY public.cursos DROP CONSTRAINT "cursos_id_aula_FK";
       public          administrador_bd    false    207    204    2946            �           2606    17733 (   material_informatico material_id_aula_FK    FK CONSTRAINT     �   ALTER TABLE ONLY public.material_informatico
    ADD CONSTRAINT "material_id_aula_FK" FOREIGN KEY (id_aula) REFERENCES public.aulas(id_aula);
 T   ALTER TABLE ONLY public.material_informatico DROP CONSTRAINT "material_id_aula_FK";
       public          administrador_bd    false    2946    211    204                  x������ � �            x������ � �            x������ � �            x������ � �            x������ � �            x������ � �            x������ � �            x������ � �         H   x�3426�,��/���4200�50"N3�H������q��T�����[&Y����&r��qqq ��     